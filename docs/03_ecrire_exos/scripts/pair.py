# --- PYODIDE:code --- #

def est_pair(nombre):
    ...


# --- PYODIDE:corr --- #

def est_pair(nombre):
    return nombre % 2 == 0


# --- PYODIDE:tests --- #

assert est_pair(2) is True
assert est_pair(2000) is true
assert est_pair(1) is False
assert est_pair(777) is False

# --- PYODIDE:secrets --- #

assert est_pair(10**10) is True
assert est_pair(10**10 + 1) is False
assert est_pair(0) is True

