---
author: Mireille Coilhac et Vincent-Xavier Jumel
title: Tout pour créer une page
---

!!! danger "Attention aux indentations"

    Les indentations de 4 espaces peuvent être réalisées dans le Web IDE en utilisant la touche de tabulation
     à condition d'avoir réalisé le réglage du Web IDE "Select indentation".
     
    👉 [Configuration du Web IDE](../10_survie/kit_gitlab.md){:target="_blank" }

## I. Syntaxe basique de Markdown

!!! info "Les titres"

    ```markdown title="Le code à copier"
    ## Titre 1 : Mon titre de niveau 1

    Mon paragraphe

    ### Titre 2 : Mon titre de niveau 2
    ```
    <div class="result" markdown>
    ## Titre 1 : Mon titre de niveau 1

    Mon paragraphe 1

    ### Titre 2 : Mon titre de niveau 2

    Mon paragraphe 2
    </div>

### 2. Les tableaux

Il est facile de réaliser des tableaux avec Markdown, mais la présentation est basique.  
Les largeurs de colonnes sont gérées automatiquement, les espaces entre les `|  |` peuvent être aléatoires.  
Il faut laisser une ligne vide avant de commencer un tableau.


!!! info "Les tableaux"

    ```markdown title="Le code à copier"
    | Titre 1  | Titre 2 | Titre 3 |
    | :---    | :----:    | ---:   |
    | aligné gauche   | centré  | aligné droite |
    | Pomme | Brocoli | Pois |
    ```
    <div class="result" markdown>

    | Titre 1  | Titre 2 | Titre 3 |
    | :---    | :----:    | ---:   |
    | aligné gauche   | centré  | aligné droite |
    | Pomme | Brocoli | Pois |
    
    </div>

Pour des tableaux plus sophistiqués, on peut les écrire en HTML : 


!!! info "Les tableaux en HTML"
    ```html title="Le code à copier"
    <table>
    <tr>
        <td style="border:1px solid; font-weight:bold;">Poires</td>
        <td style="border:1px solid; font-weight:bold;">Pommes</td>
        <td style="border:1px solid; font-weight:bold;">Oranges</td>
        <td style="border:1px solid; font-weight:bold;">Kiwis</td>
        <td style="border:1px solid; font-weight:bold;">...</td>
    </tr>
    <tr>
        <td style="border:1px solid; font-weight:bold;">1 kg</td>
        <td style="border:1px solid; font-weight:bold;">2 kg</td>
        <td style="border:1px solid; font-weight:bold;">1 kg</td>
        <td style="border:1px solid; font-weight:bold;">500 g</td>
        <td style="border:1px solid; font-weight:bold;">...</td>
    </tr>
    </table>
    ```
    <div class="result" markdown>
    <table>
    <tr>
        <td style="border:1px solid; font-weight:bold;">Poires</td>
        <td style="border:1px solid; font-weight:bold;">Pommes</td>
        <td style="border:1px solid; font-weight:bold;">Oranges</td>
        <td style="border:1px solid; font-weight:bold;">Kiwis</td>
        <td style="border:1px solid; font-weight:bold;">...</td>
    </tr>
    <tr>
        <td style="border:1px solid; font-weight:bold;">1 kg</td>
        <td style="border:1px solid; font-weight:bold;">2 kg</td>
        <td style="border:1px solid; font-weight:bold;">1 kg</td>
        <td style="border:1px solid; font-weight:bold;">500 g</td>
        <td style="border:1px solid; font-weight:bold;">...</td>
    </tr>
    </table>
    </div>


### 3. Emojis qui peuvent être utiles


🌐 
👉
⚠️
🌵
💡
🤔
🌴
😂
🤿
😀
✏️
📝
😢
🐘
🖐️
👓
👗
👖
⌛
😊
🖲️
🏳️
😴
💻
👀

### 4. Insérer une image

A partir de Web IDE : Créer un dossier `images`. Télécharger dans ce dossier les images.

!!! info "Image à gauche"

    ```markdown title="Le code à copier"
    ![nom image](images/paysage_reduit.jpg){ width=20% }

    Par défaut l'image est placée à gauche.
    ```
    <div class="result" markdown>

    ![nom image](images/paysage_reduit.jpg){ width=20% }

    Par défaut l'image est placée à gauche.

    </div>


!!! info "Image à droite"

    ```markdown title="Aligner une image à droite"
    ![](images/paysage_reduit.jpg){ width=10%; align=right }

    Le texte se place **à gauche** de mon image placée à droite.
    ```
    <div class="result" markdown>

    ![](images/paysage_reduit.jpg){ width=10%; align=right }

    Le texte se place **à gauche** de mon image placée à droite.

    </div>

!!! info "3 images côtes à côtes"

    ```markdown title="Le code à copier"
    ![](images/paysage_reduit.jpg){ width=5% }
    ![](images/paysage_reduit.jpg){ width=10% }
    ![](images/paysage_reduit.jpg){ width=15% }
    ```
    <div class="result" markdown>

    ![](images/paysage_reduit.jpg){ width=5% }
    ![](images/paysage_reduit.jpg){ width=10% }
    ![](images/paysage_reduit.jpg){ width=15% }

    </div>

 
!!! info "Image centrée"

    ```markdown title="Le code à copier"
    ![paysage](images/paysage_reduit.jpg){ width=10%; : .center }

    Le texte se place **en dessous** de l'image centrée.
    ```
    <div class="result" markdown>
    ![paysage](images/paysage_reduit.jpg){ width=10%; : .center }

    Le texte se place **en dessous** de l'image centrée.
    </div>


!!! info "Image et mode sombre"

    GeoGebra par exemple permet d'exporter des images au format svg.  
    Ici, nous avons l'image `inverse.svg` dans le dossier `images`.  
    Pour qu'elle apparaîsse correctement en mode sombre, il faut ajouter l'option `.autolight`


    ```markdown title="Le code à copier"
    ![Fonction inverse](images/inverse.svg){.center .autolight width=30%}
    ```

    <div class="result" markdown>
    ![Fonction inverse](images/inverse.svg){.center .autolight width=30%}
    </div>

### 5. Ecriture de code

!!! info "Les backticks"

    Nous utilisons pour cela un ou trois **backticks** (ou **apostrophe inversée**). Attention, à ne pas le confondre un **backtick** avec un guillemet. On le trouve généralement avec les touche <kbd>ALT GR</kbd> + <kbd>è</kbd> du clavier.


!!! info "Code au fil d'un texte inline"

    ```markdown title="Le code à copier"
    La variable `nombres` est de type `list`.
    ```
    <div class="result" markdown>
    La variable `nombres` est de type `list`.
    </div>

!!! info "Dans une console Python"

    ````markdown title="Le code à copier"
    ```pycon
    >>> nombres = [3, 8, 7]
    >>>fruits = ['Poire', 'Pomme', 'Orange', 'Kiwi']
    ```
    ````
    <div class="result" markdown>
    ```pycon
    >>> nombres = [3, 8, 7]
    >>> fruits = ['Poire', 'Pomme', 'Orange', 'Kiwi']
    ```
    </div>

!!! info "Dans un éditeur Python"

    ````markdown title="Le code à copier"
    ```python
    print("Hello World")
    ```
    ````
    <div class="result" markdown>
    ```python
    print("Hello World")
    ```
    </div>

!!! info "Dans un éditeur Python avec numéros de lignes"

    ````markdown title="Le code à copier"
    ```python linenums='1'
    a = 1
    b = 2
    t = b
    b = a
    a = t
    ```
    ````
    <div class="result" markdown>
    ```python linenums='1'
    a = 1
    b = 2
    t = b
    b = a
    a = t
    ```
    </div>

!!! info "Coloration syntaxique Python Inline"

    ```markdown title="Le code à copier"
    Nous allons utiliser le type `#!py list` de Python
    ```
    <div class="result" markdown>
    Nous allons utiliser le type `#!py list` de Python
    </div>

  
### 6. Les touches du clavier

!!! info "Une première méthode pour faire apparaître des touches du clavier"

    ```markdown title="Le code à copier"
    Nous allons utiliser la touche <kbd>%</kbd>
    ```
    <div class="result" markdown>
    Nous allons utiliser la touche <kbd>%</kbd>
    </div>

!!! info "Une deuxième méthode pour faire apparaître des touches du clavier"

    On peut trouver beaucoup de touches ici : [Touches du clavier](https://facelessuser.github.io/pymdown-extensions/extensions/keys/#overview){:target="_blank" }

    Voici des exemples d'utilisation :

    ```markdown title="Le code à copier"
    ++ctrl+alt+del++

    ++enter++
    ```
    <div class="result" markdown>
    ++ctrl+alt+del++

    ++enter++
    </div>

### 7. Des "trous dans le texte"

!!! info "Les espaces"

    Pour faire apparaître des espaces dans un texte, mettre des espaces ne suffit pas .

    ```markdown title="Le code avec espace"
    Par exemple : `a == 3` est                 booléenne.
    ```
    <div class="result" markdown>
    Par exemple : `a == 3` est                 booléenne.
    </div>

!!! info "une possibilité pour ajouter des espaces"

    ```markdown title="Le code avec espace"
    Par exemple : `a == 3` est $\hspace{10em}$ booléenne.
    ```

    <div class="result" markdown>
    Par exemple : `a == 3` est $\hspace{10em}$ booléenne.
    </div>


## II. Les admonitions

Toutes les admonitions ont un titre par défaut. Pour personnaliser un titre, il suffit de le rajouter entre guillemets.

!!! info "Admonition info"
    ````markdown title="Le code à copier"
    !!! info "Mon info"
        
        Ma belle info qui doit être indentée
    ````
    <div class="result" markdown>
    !!! info "Mon info"
        
        Ma belle info qui doit être indentée
    </div>


!!! info "Admonition remarque"

    ```markdown title="Le code à copier"
    !!! warning "Remarque"

        Ma remarque qui doit être indentée
    ```
    <div class="result" markdown>
    !!! warning "Remarque"

        Ma remarque qui doit être indentée
    </div>


!!! info "Admonition remarque repliée"

    ```markdown title="Le code à copier"
    ??? warning "Remarque"

        Ma remarque qui doit être indentée
    ```
    <div class="result" markdown>
    ??? warning "Remarque"

        Ma remarque qui doit être indentée
    </div>
    

!!! info "Admonition attention"
    ```markdown title="Le code à copier"
    !!! danger "Attention"

        texte indenté   
    ```
    <div class="result" markdown>
    !!! danger "Attention"

        texte indenté
    </div>


!!! info "Admonition attention repliée"
    ```markdown title="Le code à copier"
    ??? danger "Attention"

        texte indenté   
    ```
    <div class="result" markdown>
    ??? danger "Attention"

        texte indenté
    </div>


!!! info "Admonition note à dérouler"
    ```markdown title="Le code à copier"
    ??? note "se déroule en cliquant dessus"

        Ma note indentée
    ```
    <div class="result" markdown>
    ??? note "se déroule en cliquant dessus"

        Ma note indentée
    </div>


!!! info "Admonition astuce à dérouler"
    ```markdown title="Le code à copier"
    ??? tip "Astuce"

        Ma belle astuce indentée
    ```
    <div class="result" markdown>
    ??? tip "Astuce"

        Ma belle astuce indentée
    </div>


!!! info "Admonition résumé"
    ```markdown title="Le code à copier"
    !!! abstract "Résumé"

        Mon bilan indenté
    ```
    <div class="result" markdown>
    !!! abstract "Résumé"

        Mon bilan indenté 
    </div>


!!! info "Admonition note repliable dépliée"
    ```markdown title="Le code à copier"
    ???+ note dépliée

        Mon texte indenté
    ```
    <div class="result" markdown>
    ???+ note dépliée

        Mon texte indenté
    </div>


!!! info "Admonition note à cliquer"
    ```markdown title="Le code à copier"
    ??? note pliée "Note à cliquer"

        Mon texte indenté qui se dévoilera après le clic  
    ```
    <div class="result" markdown>
    ??? note pliée "Note à cliquer"

        Mon texte indenté qui se dévoilera après le clic
    </div>


!!! info "Admonition échec"
    ```markdown title="Le code à copier"
    !!! failure "Echec"

        Mon texte indenté
    ```
    <div class="result" markdown>
    !!! failure "Echec"

        Mon texte indenté
    </div>


!!! info "Admonition bug"
    ```markdown title="Le code à copier"
    !!! bug "Bug"

        Mon texte indenté
    ```
    <div class="result" markdown>
    !!! bug
        
        Mon texte indenté
    </div>


!!! info "Admonition exemple"
    ```markdown title="Le code à copier"
    !!! example "Exemple"

        Mon exemple indenté
    ```
    <div class="result" markdown>
    !!! example "Exemple"

        Mon exemple indenté
    </div>


!!! info "Admonition note dans la marge gauche"
    ```markdown title="Le code à copier"
    !!! note inline "Note à gauche"

        Texte de la note indenté
        
    Le texte non indenté se place à droite de la note.  
    Il est en dehors de l'admonition.  
    Il doit être assez long  
    ...
    ```
    <div class="result" markdown>
    !!! note inline "Note à gauche"

        Texte de la note indenté
        
    Le texte non indenté se place à droite de la note.  
    Il est en dehors de l'admonition.  
    Il doit être assez long  
    ...
    </div>


!!! info "Admonition note dans la marge droite"
    ```markdown title="Le code à copier"

    !!! note inline end "Note à droite"

        Texte de la note indenté
        
    Le texte non indenté se place à droite de la note.  
    Il est en dehors de l'admonition.  
    Il doit être assez long  
    ...
    ```
    <div class="result" markdown>
    !!! note inline end "Note à droite"

        Texte de la note indenté
    
    Le texte non indenté se place à droite de la note.  
    Il est en dehors de l'admonition.  
    Il doit être assez long  
    ...
    </div>

!!! info "Admonition exercice"

    Voir le VI. Exercice suivi d'une solution qui se découvre en cliquant


!!! info "Panneaux coulissants"

    ```markdown title="Le code à copier"
    !!! example "Exemples avec trois panneaux"

        === "Panneau 1"
            Ici du texte concernant ce panneau 1

            Il peut prendre plusieurs lignes

        === "Panneau 2"
            Ici du texte concernant ce panneau 2

            Il peut prendre plusieurs lignes

        === "Panneau 3"
            Ici du texte concernant ce panneau 3

            Il peut prendre plusieurs lignes

            On peut aussi mettre une image : 

            ![nom image](images/paysage_reduit.jpg){ width=20% }
    ```  
    <div class="result" markdown>

    !!! example "Exemples avec trois panneaux"

        === "Panneau 1"
            Ici du texte concernant ce panneau 1

            Il peut prendre plusieurs lignes

        === "Panneau 2"
            Ici du texte concernant ce panneau 2

            Il peut prendre plusieurs lignes

        === "Panneau 3"
            Ici du texte concernant ce panneau 3

            Il peut prendre plusieurs lignes

            On peut aussi mettre une image : 

            ![nom image](images/paysage_reduit.jpg){ width=20% }


    </div>




## III. Faire des liens 

Ce qui est entre crochets comme `[link text]` est personnalisable.

### 1. Liens externes

!!! info "Lien externe sans bouton"

    ```markdown title="Le code à copier"
    [Exercices Python en ligne](https://codex.forge.apps.education.fr/)
    ```

    <div class="result" markdown>

    [Exercices Python en ligne](https://codex.forge.apps.education.fr/)

    </div>

!!! info "Lien externe sans bouton dans une autre fenêtre"

    ```markdown title="Le code à copier"
    [Exercices Python en ligne](https://codex.forge.apps.education.fr/){:target="_blank" }
    ```

    <div class="result" markdown>

    [Exercices Python en ligne](https://codex.forge.apps.education.fr/){:target="_blank" }

    </div>



!!! info "Lien externe avec bouton dans une autre fenêtre"

    ```markdown title="Le code à copier"
    [Exercices Python en ligne](https://codex.forge.apps.education.fr/){ .md-button target="_blank" rel="noopener" }
    ```

    <div class="result" markdown>

    [Exercices Python en ligne](https://codex.forge.apps.education.fr/){ .md-button target="_blank" rel="noopener" }

    </div>

### 2. Liens internes

!!! info "Lien vers un article du **même** répertoire"

    Lien vers le fichier `mon_article.md` du **même** répertoire.
 
    ```markdown title="Le code à copier"
    [link text](mon_article.md)
    ```
    <div class="result" markdown>

    [link text](mon_article.md)

    </div>

!!! info "Lien vers un article du **répertoire parent** du répertoire courant"

    Lien vers le fichier `mon_article.md`.

    ```markdown title="Le code à copier"
    [Mon lien](../mon_article.md)
    ```
    <div class="result" markdown>

    [Mon lien](../mon_article.md)

    </div>

!!! info "Lien vers un article d’un **sous-répertoire** du répertoire courant"

    Lien vers le fichier `mon_article.md` du sous-répertoire `directory`.

    ```markdown title="Le code à copier"
    [link text](directory/mon_article.md)
    ```
    <div class="result" markdown>

    [link text](directory/mon_article.md)

    </div>


👉 Le suivant est le seul qui fonctionne ici :

!!! info "Lien vers un article d’un **sous-répertoire du répertoire parent du répertoire actif**"

    Lien vers le fichier `exercices.md` du sous-répertoire `03_ecrire_exos` du répertoire parent du répertoire actif.


    ```markdown title="Le code à copier"
    [Ecrire des exercices](../03_ecrire_exos/exercices.md)
    ```
    <div class="result" markdown>

    [Ecrire des exercices](../03_ecrire_exos/exercices.md)

    </div>


### 3. Liens de téléchargements de fichiers

Mettre le fichier `file_telecharger` dans un dossier `a_telecharger`. Bien indiquer le bon chemin comme dans l'exemple ci-dessous

!!! info "Mon fichier à donner en téléchargement"

    ```markdown title="Le code à copier"
    
    🌐 Fichier à télécharger :

    Fichier `file_telecharger` : [Clic droit, puis "Enregistrer la cible du lien sous"](a_telecharger/file_telecharger)

    ```
    <div class="result" markdown>

    🌐 Fichier à télécharger :
    
    Fichier `file_telecharger` : [Clic droit, puis "Enregistrer la cible du lien sous"](a_telecharger/file_telecharger)

    </div>



## IV. Insertion de vidéo

!!! info "Insérer une vidéo en ligne"

    ```markdown title="Le code à copier"
    <video controls src="https: ..."></video>
    ```

    Exemple : 

    ```markdown title="Le code à copier"
    <video controls src="https://upload.wikimedia.org/wikipedia/commons/d/d2/See_a_Koala_scratch%2C_yawn_and_sleep.webm"></video>

    _Source : Paolo Leone: https://www.youtube.com/channel/UCFRQrtHw36NNpX1oXpHMfRw, CC BY-SA 4.0 <https://creativecommons.org/licenses/by-sa/4.0>, via Wikimedia Commons_
    ```

    <div class="result" markdown>
    <video controls src="https://upload.wikimedia.org/wikipedia/commons/d/d2/See_a_Koala_scratch%2C_yawn_and_sleep.webm"></video>
    
    _Source : Paolo Leone: https://www.youtube.com/channel/UCFRQrtHw36NNpX1oXpHMfRw, CC BY-SA 4.0 <https://creativecommons.org/licenses/by-sa/4.0>, via Wikimedia Commons_
    </div>

👉 Il suffit le plus souvent de recopier le code qui est proposé dans "partager" et "intégrer" de la vidéo que l'on veut mettre.

!!!example "Exemple d'une vidéo de Portail tubes de apps.education.fr"
    ```html
    <iframe title="Présentation de CodiMD (et du code Markdown) en moins de 2 minutes" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/5a1256f5-a3cd-4798-84f9-3dd7b8f01aac" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>
    ```
    <div class="result" markdown>
    <iframe title="Présentation de CodiMD (et du code Markdown) en moins de 2 minutes" width="560" height="315" src="https://tube-sciences-technologies.apps.education.fr/videos/embed/5a1256f5-a3cd-4798-84f9-3dd7b8f01aac" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>
    </div>

## V. QCM

!!! info "Barrer des réponses fausses"

    Mettre un texte entre ~~ et ~~ permet de le barrer.

!!! info "Créer un QCM avec des volets"

    ```markdown title="Le code à copier"
    ???+ question

        Voici diverses propositions

        === "Cocher la ou les affirmations correctes"
                
            - [ ] Proposition 1
            - [ ] Proposition 2
            - [ ] Proposition 3
            - [ ] Proposition 4

        === "Solution"
                
            - :x: ~~Proposition 1~~ Optionnel : Faux car ... 
            - :white_check_mark: Proposition 2 . Optionnel : Juste car ...
            - :white_check_mark: Proposition 3 . Optionnel : Juste car ...
            - :x: ~~Proposition 4~~ Optionnel : Faux car ... 
    ```
    <div class="result" markdown>

    ???+ question

        Voici diverses propositions

        === "Cocher la ou les affirmations correctes"
                
            - [ ] Proposition 1
            - [ ] Proposition 2
            - [ ] Proposition 3
            - [ ] Proposition 4

        === "Solution"
                
            - :x: ~~Proposition 1~~ Optionnel : Faux car ... 
            - :white_check_mark: Proposition 2 . Optionnel : Juste car ...
            - :white_check_mark: Proposition 3 . Optionnel : Juste car ...
            - :x: ~~Proposition 4~~ Optionnel : Faux car ... 

    </div>


!!! info "Créer un QCM avec validation, rechargement et score"

    Il faut créer un fichier avec l'extension `.json`, par exemple nommé `mon_qcm.json` qui contient tout le QCM avec les différentes options souhaitées. 

    👉 Pour créer **très facilement de façon automatique** ce fichier, suivre ce lien : [Création de QCM : générer le fichier .json automatiquement](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/redactors/qcm_builder/){ .md-button target="_blank" rel="noopener" }. 

    👉 On peut remplir les champs en écrivant en Markdown, et donc y inclure **du code**, **des images** ou des  [formules écrites en LaTex](../latex/formules_latex.md){:target="_blank" }
    
    Une fois tous les champs remplis (on peut agrandir les zones d'écritures en les étirant en bas à droite), il suffit de récupérer le fichier fourni grâce aux deux boutons situés en bas.

    Le fichier doit ensuite être placé dans le répertoire du fichier markdown où l'on veut insérer le QCM (ou dans un sous-répertoire), et il suffira alors d'ajouter la macro suivante au fichier markdown : 

    ```markdown title="Le code à copier"
    {% raw %}
    {{ multi_qcm('mon_qcm.json') }}
    {% endraw %}
    ```

    👉 Le chemin vers le fichier `.json` dans l’appel de macro est **relatif** au fichier Markdown en cours.  
        Par exemple si  le fichier json se trouve dans le sous-dossier `./qcms/mon_qcm.json`, mettre :

    ```markdown title="Le code à copier"
    {% raw %}
    {{ multi_qcm('qcms/mon_qcm.json') }}
    {% endraw %}
    ```



!!! info "Un exemple"

    ??? note "Fichier Json utilisé pour cet exemple"

        Le fichier Json utilisé contient le code suivant :

        ```python title="qcm_exemple.json"
        {
          "questions": [
            [
              "```python title=\"\"\nn = 8\nwhile n > 1:\n    n = n/2\n```\n\nQue vaut `n` après l'exécution du code ?",
              [
                "0.5",
                "1.0",
                "2.0",
                "4.0"
              ],
              [2]
            ],
            [
              "`meubles = ['Table', 'Commode', 'Armoire', 'Placard', 'Buffet']`\n\nCocher toutes les bonnes réponses.",
              [
                "`meubles[-1]` vaut `'Buffet'`",
                "`meubles[1]` vaut `'Table'`",
                "`meubles[1]` vaut `'Commode'`"
              ],
              [1,3],
              {"multi":false}
            ]
          ],
          "description": "QCM Exemple",
          "shuffle": true
        }

        ```

    Ce fichier Json a été généré [avec l'outil en ligne](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/redactors/qcm_builder/){ target="_blank" rel="noopener" } de la documentation de Pyodide-MkDocs-Theme, en remplissant les champs comme ceci : 

    ![Création du fichier en ligne](images/ecrire_qcm_legende.png){ width=50% }

    Une fois le fichier placé dans le répertoire du fichier Markdown, le QCM est inséré dans la page avec l'appel suivant :

    ```markdown title="Le code à copier"
    {% raw %}
    {{ multi_qcm('qcm_exemple.json') }}
    {% endraw %}
    ```
    <div class="result" markdown>

    {{ multi_qcm('qcm_exemple.json') }}

    </div>



!!! info "Tout sur les QCM"

    [Documentation officielle pour les QCM](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/redactors/qcms/){ .md-button target="_blank" rel="noopener" }


## VI. Exercice suivi d'une solution qui se découvre en cliquant

### 1. Exercice sans code

!!! info "Exercice sans code Python"

    ```markdown title="Le code à copier"
    ???+ question "Mon exercice"

        **1.** Question 1.   
        Texte de la question indenté

        ??? success "Solution"

            Ici se trouve la solution rédigée de la question 1.
            Le texte doit être indenté dans cette nouvelle admonition

        **2.** Question 2.   
        Texte de la question indenté

        ??? success "Solution"

            Ici se trouve la solution rédigée de la question 2.
            Le texte doit être indenté dans cette nouvelle admonition
    ```
    <div class="result" markdown>

    ???+ question "Mon exercice"

        **1.** Question 1.   
        Texte de la question indenté

        ??? success "Solution"

            Ici se trouve la solution rédigée de la question 1.
            Le texte doit être indenté dans cette nouvelle admonition

        **2.** Question 2.   
        Texte de la question indenté

        ??? success "Solution"

            Ici se trouve la solution rédigée de la question 2.
            Le texte doit être indenté dans cette nouvelle admonition

    </div>

### 2. Exercice avec code : un tout premier exemple


!!! info "Un IDE avec du code à compléter"

    ````markdown title="Le code à copier"
    ???+ question

        Compléter le script ci-dessous :
        Mettre le fichier python dans un dossier scripts

        {% raw %}
        {{ IDE('scripts/mon_fichier') }}
        {% endraw %}

        ??? success "Solution"

            On peut donner la solution sous forme de code à copier :
            ```python
            # Code de la solution
            ```       
    ````
    <div class="result" markdown>

    ???+ question

        Compléter le script ci-dessous :
        Mettre le fichier python dans un dossier scripts

        {{ IDE('scripts/mon_fichier') }}

        ??? success "Solution"
            On peut donner la solution sous forme de code à copier :
            ```python
            # Code de la solution
            ``` 
    </div>



### 3. D'autres exemples

Pour en savoir plus, voir la rubrique [Ecrire des exercices](../03_ecrire_exos/exercices.md/){ .md-button target="_blank" rel="noopener" }  

Vous y trouverez des exemples d'exercices avec des réponses dans des IDE, qui peuvent s'afficher après plusieurs tentatives de l'élève, et plein d'autres possibilités.
    
        
## VII. Notebooks pythons : sujet à télécharger et corrections cachées ou visibles

On peut évidemment mettre en ligne un Notebook Python, et faire travailler les élèves dessus.

Mettre le fichier `mon_fichier_sujet.ipynb` dans un dossier `a_telecharger`. Bien indiquer le bon chemin comme dans l'exemple ci-dessous

!!! info "Mon sujet sur notebook à télécharger avec correction cachée"

    ```markdown title="Le code à copier"
    ???+ question "Mon sujet sur notebook à télécharger avec correction cachée"

        Après avoir téléchargé le fichier, vous pourrez le lire à partir de [Basthon](https://notebook.basthon.fr/){ .md-button target="_blank" rel="noopener" }

        🌐 TD à télécharger : Fichier `mon_fichier_sujet.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/mon_fichier_sujet.ipynb)

        ⏳ La correction viendra bientôt ... 

    <!--- La correction à télécharger plus tard A SORTIR DE L'ADMONITION
    👉 Bien sortir ce commentaire de l'admonition en supprimant l'indentation
    🌐 Fichier `mon_fichier_corr.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/mon_fichier_corr.ipynb)
    -->
    ```
    <div class="result" markdown>

    ???+ question "Mon sujet sur notebook à télécharger avec correction cachée"

        Après avoir téléchargé le fichier, vous pourrez le lire à partir de [Basthon](https://notebook.basthon.fr/){ .md-button target="_blank" rel="noopener" }

        🌐 TD à télécharger : Fichier `mon_fichier_sujet.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/mon_fichier_sujet.ipynb)

        ⏳ La correction viendra bientôt ... 

    <!--- La correction à télécharger plus tard A SORTIR DE L'ADMONITION
    👉 Bien sortir ce commentaire de l'admonition en supprimant l'indentation
    🌐 Fichier `mon_fichier_corr.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/mon_fichier_corr.ipynb)
    -->

    </div>

Si l'on veut dévoiler la correction, il suffit de supprimer la syntaxe qui la cachait dans un commentaire.

!!! info "Mon sujet sur notebook à télécharger avec correction visible"

    ```markdown title="Le code à copier"
    ???+ question "Mon sujet sur notebook à télécharger avec correction visible"

        Après avoir téléchargé le fichier, vous pourrez le lire à partir de [Basthon](https://notebook.basthon.fr/){ .md-button target="_blank" rel="noopener" }

        🌐 TD à télécharger : Fichier `mon_fichier_sujet.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/mon_fichier_sujet.ipynb)

        🌐 Correction à télécharger : Fichier `mon_fichier_corr.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/mon_fichier_corr.ipynb)
    ```
    <div class="result" markdown>

    ???+ question "Mon sujet sur notebook à télécharger avec correction visible"

        Après avoir téléchargé le fichier, vous pourrez le lire à partir de [Basthon](https://notebook.basthon.fr/){ .md-button target="_blank" rel="noopener" }

        🌐 TD à télécharger : Fichier `mon_fichier_sujet.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/mon_fichier_sujet.ipynb)

        🌐 Correction à télécharger : Fichier `mon_fichier_corr.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/mon_fichier_corr.ipynb)

    </div>


## VIII. Exercices en lignes

### Utiliser un lien

Il existe beaucoup d'exercices en ligne, il suffit de donner le lien (par exemple sur le site [CodEx](https://codex.forge.apps.education.fr/){:target="_blank" } )


!!! info "Bouton de lien vers un exercice"

    ```markdown title="Le code à copier"
    ???+ question "Lectures dans deux tableaux (1) - non guidé"

        Les éléments d'un tableau sont-ils tous différents ?

        [Éléments tous différents](https://codex.forge.apps.education.fr/exercices/tous_differents/){ .md-button target="_blank" rel="noopener" }
    ```
    <div class="result" markdown>

    ???+ question "Lectures dans deux tableaux (1) - non guidé"

        Les éléments d'un tableau sont-ils tous différents ?

        [Éléments tous différents](https://codex.forge.apps.education.fr/exercices/tous_differents/){ .md-button target="_blank" rel="noopener" }

    </div>

### Intégrer un exercice directement dans le site

Le site WIMS met à disposition beaucoup de ressources interactives dans de nombreux domaines : Biologie, Chimie, Informatique, Langues, Mathématiques, Physique, et autres domaines encore (disciplines artistiques, Géographie, Sciences de la terre, Histoire, Sciences, Ecogestion, Éducation physique, Astronomie, Sciences et techniques industrielles)

[Contenu pédagogique de WIMS](https://euler-ressources.ac-versailles.fr/wims/wims.cgi?session=9G03F5B8EF_help.3&+lang=fr&+module=adm%2Fbrowse&+cmd=new&+job=subject){ .md-button target="_blank" rel="noopener" }

??? note "Chercher un exercice WIMS"

    !!! info "En regardant les thèmes proposés"

        * Suivre le lien suivant : [Contenu pédagogique de WIMS](https://euler-ressources.ac-versailles.fr/wims/wims.cgi?session=9G03F5B8EF_help.3&+lang=fr&+module=adm%2Fbrowse&+cmd=new&+job=subject){:target="_blank" }
        * Sélectionner l'onglet voulu
        * Cliquer sur le thème choisi puis **descendre au bas de la page** et explorer ce qui est proposé dans l'onglet "Modules d'exercices"

    !!! info " En utilisant la barre de recherche"

        * Suivre le lien suivant : [Recherche dans WIMS](https://euler-ressources.ac-versailles.fr/wims/){:target="_blank" }
        * Descendre au bas de la page. En dessous des cadres "Actualités-Exemples", compléter la barre de recherche.

        ![recherche wims](images/recherche_wims.png){ width=70% }

        * Les résultats apparaîssent en bas de page.

??? note "Intégrer un exercice dans votre site"
    
    * Cliquer sur un module d'exercices dans l'onglet "Modules d'exercices".
    * Dans le cadre "Choix des exercices", cliquer sur l'exercice voulu. On intègre dans le site **un seul exercice à la fois**.
    * Modifier si nécessaire les paramétrages proposés en dessous.
    * Tout en bas Cliquer sur le bouton <kbd>Au travail</kbd>.
    * La page de l'exercice choisi apparaît. En haut à gauche cliquer sur "À propos".

    ![A propos](images/a_propos_wims.png){ width=30% }

    * Dans la colonne de gauche du cadre "Informations sur cet exercice", à la ligne "Lecteur exportable" , recopier tout ce qui se trouve dans le paragraphe : 
    "Afficher la config actuelle  sur un site / un blog ("light" version):"

    ![iframe](images/iframe_wims.png){ width=50% }

    !!! info "Exemple d'exercice sur les listes en Python"

        ```markdown title="Le code à copier"
        <iframe src="https://euler-ressources.ac-versailles.fr/wims/wims.cgi?module=adm/raw&job=lightpopup&emod=H4/algo/oefpython.fr&parm=cmd=new;exo=liste_ind_val2;qnum=1;scoredelay=;seedrepeat=0;qcmlevel=1&option=noabout" width="100%" height="600"></iframe>
        ```
        <div class="result" markdown>
        <iframe src="https://euler-ressources.ac-versailles.fr/wims/wims.cgi?module=adm/raw&job=lightpopup&emod=H4/algo/oefpython.fr&parm=cmd=new;exo=liste_ind_val2;qnum=1;scoredelay=;seedrepeat=0;qcmlevel=1&option=noabout" width="100%" height="600"></iframe>
        </div>


[Documentation officielle de WIMS](https://nuage03.apps.education.fr/index.php/s/yrRk8HmxjmfAzf6){ .md-button target="_blank" rel="noopener" }


## IX. Ecriture de code : compléments

!!! info "Annotations première présentation"

    !!! danger "Attention"

        Bien veiller à mettre une ligne vide après la balise fermante ```   et avant les annotations numérotées


    L'exemple suivant est donné avec Python, mais peut servir avec n'importe quel langage (avec ````html```` par exemple)

    ````markdown title="Le code à copier"
    ```python 
    note = float(input("Saisir votre note : "))
	if note >= 16:  # (1)
    	print("TB")
	elif note >= 14:  # (2)
    	print("B")
	elif note >= 12:  # (3)
    	print("AB")
	elif note >= 10:
    	print("reçu")
	else:
    	print("refusé")
    ```

    1. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.

    2. :warning: `elif` signifie **sinon si**.

    3. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.

    !!! warning "Prenez le temps de lire les commentaires (cliquez sur les +)"
    ````
    <div class="result" markdown>

    ```python 
    note = float(input("Saisir votre note : "))
	if note >= 16:  # (1)
    	print("TB")
	elif note >= 14:  # (2)
    	print("B")
	elif note >= 12:  # (3)
    	print("AB")
	elif note >= 10:
    	print("reçu")
	else:
    	print("refusé")
    ```

    1. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.

    2. :warning: `elif` signifie **sinon si**.

    3. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.

    !!! warning "Prenez le temps de lire les commentaires (cliquez sur les +)"

    </div>


!!! info "Annotations autre présentation"

    ````markdown title="Le code à copier"
    !!! warning inline end "Important"

        Prenez le temps de lire les commentaires !
    
        Cliquez sur les +


    ```python
	    note = float(input("Saisir votre note : "))
	    if note >= 16:  # (1)
    	    print("TB")
	    elif note >= 14:  # (2)
    	    print("B")
	    elif note >= 12:  # (3)
    	    print("AB")
	    elif note >= 10:
    	    print("reçu")
	    else:
    	    print("refusé")
    ```

    1. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.

    2. :warning: `elif` signifie **sinon si**.

    3. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.
    ````
    <div class="result" markdown>
    !!! warning inline end "Important"

        Prenez le temps de lire les commentaires !
    
        Cliquez sur les +


    ```python
	    note = float(input("Saisir votre note : "))
	    if note >= 16:  # (1)
    	    print("TB")
	    elif note >= 14:  # (2)
    	    print("B")
	    elif note >= 12:  # (3)
    	    print("AB")
	    elif note >= 10:
    	    print("reçu")
	    else:
    	    print("refusé")
    ```

    1. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.

    2. :warning: `elif` signifie **sinon si**.

    3. :warning: Le caractère `#!py :` est **obligatoire** après `if`, `elif`, `else`, `for` et `while`. Il provoque l'indentation automatique du bloc d'instruction qui suit.
    </div>

!!! info "Surlignage de lignes de code"

    Vous pouvez surligner des lignes, consécutives ou non, en couleur. 

    ````markdown title="Le code à copier"
    ```python hl_lines="1 3-5"
    def trouve_max(tableau):
        maximum = tableau[0]
        for valeur in tableau:
            if valeur > maximum:
                maximum = valeur
        return maximum
    ```
    ````

    <div class="result" markdown>
    ```python hl_lines="1 3-5"
    def trouve_max(tableau):
        maximum = tableau[0]
        for valeur in tableau:
            if valeur > maximum:
                maximum = valeur
        return maximum
    ```
    </div>

## X. Les arbres 

Il suffit de numéroter les noeuds : n0, n1, n2 etc. avec les étiquettes entre parenthèses, et les flèches pour les arcs entre les noeuds.
En 1er à gauche.

!!! info "Un arbre"

    ````markdown title="Le code à copier"
    ```mermaid
        %%{init: {'themeVariables': {'fontFamily': 'monospace'}}}%%
        flowchart TB
            n0(15) --> n1(6)
            n1 --> n3(1)
            n1 --> n4(10)
            n0 --> n2(30)
            n2 --> n8[Null]
            n2 --> n5(18)
            n5 --> n6(16)
            n5 --> n7(25)
    ```
    ````
    <div class="result" markdown>

    ```mermaid
        %%{init: {'themeVariables': {'fontFamily': 'monospace'}}}%%
        flowchart TB
            n0(15) --> n1(6)
            n1 --> n3(1)
            n1 --> n4(10)
            n0 --> n2(30)
            n2 --> n8[Null]
            n2 --> n5(18)
            n5 --> n6(16)
            n5 --> n7(25)
    ```
    </div>


!!! info "Arbres avec certains nœuds absents "

    Voir l'exemple qui suit :

    * On numérote l'arc à ne pas représenter (le 1er est le numéro 0), et on utilise par exemple pour le numéro 3 la syntaxe :  
    `linkStyle 3 stroke-width:0px;`
    * On met les nœuds vides avec `opacity:0` :  
    `style E opacity:0;`
    

!!! info "Des nœuds absents mais leur espace conservé"

    ````markdown title="Le code à copier"
    ```mermaid
    flowchart TD
        A(12) --- B(10)
        A --- C(15)
        B --- D(5)
        B --- E( )
        D --- F(4)
        D --- G(8)
        C --- H( )
        C --- I(20)
        linkStyle 3 stroke-width:0px;
        linkStyle 6 stroke-width:0px;
        style E opacity:0;
        style H opacity:0;
    ```
    ````
    <div class="result" markdown>

    ```mermaid
    flowchart TD
        A(12) --- B(10)
        A --- C(15)
        B --- D(5)
        B --- E( )
        D --- F(4)
        D --- G(8)
        C --- H( )
        C --- I(20)
        linkStyle 3 stroke-width:0px;
        linkStyle 6 stroke-width:0px;
        style E opacity:0;
        style H opacity:0;
    ```
    </div>

!!! info "Placement des nœuds définis"

    Dans le cas des arbres binaires de recherche par exemple, on a besoin que les nœuds soient représentés dans un ordre bien précis.
    Il suffit pour cela de les donner dans le code mermaid dans l'ordre de ce qui serait le parcours en largeur de l'arbre (par niveau).

    ````markdown title="Le code à copier"
    ```mermaid
    graph TD
    A(5)
    B(2)
    C(8)
    F( )
    G( )
    D(6)
    E(9)
    H( )
    I( )
    J( )
    K( )
    A --> B
    A --> C
    C --> D
    C --> E
    B --> F
    B --> G
    D --> H
    D --> I
    E --> J
    E --> K
    ```
    ````
    <div class="result" markdown>

    ```mermaid
    graph TD
    A(5)
    B(2)
    C(8)
    F( )
    G( )
    D(6)
    E(9)
    H( )
    I( )
    J( )
    K( )
    A --> B
    A --> C
    C --> D
    C --> E
    B --> F
    B --> G
    D --> H
    D --> I
    E --> J
    E --> K
    ```
    </div>


[Lien vers différentes options de nœuds et flèches](https://coda.io/@leandro-zubrezki/diagrams-and-visualizations-using-mermaid/flowcharts-3){ .md-button target="_blank" rel="noopener" }


[lien vers la documentation mermaid - essais en ligne de mermaid](https://mermaid.live/edit#pako:eNpVkE1qw0AMha8itGohvoAXgcZus0lJodl5vBAeOTOk88NYpgTbd-84ptBqJfS-J6Q3YRc0Y4nXRNHApVYecr00lUl2EEdDC0Wxn48s4ILn-wyHp2OAwYQYrb8-b_xhhaCaTivGIMb627JJ1cN_9jxD3ZwoSojtX-XyHWZ4beyHyev_KyZxdr01PZU9FR0lqCi1uEPHyZHV-expNSgUw44VlrnV3NP4JQqVXzJKo4TPu--wlDTyDseoSbi2lB92v0PWVkJ635J4BLL8AM-3Wn4){ .md-button target="_blank" rel="noopener" }

!!! info "Un arbre horizontal"

    ````markdown title="Le code à copier"
    ```mermaid
    graph LR
	    A( )
	    B(0)
	    C(1)
	    D(0)
	    E(1)
	    F(0)
	    G(1)
	    A --- B
	    A --- C
	    B --- D
	    B --- E
	    C --- F
	    C --- G
    ```
    ````

    <div class="result" markdown>

    ```mermaid
    graph LR
	    A( )
	    B(0)
	    C(1)
	    D(0)
	    E(1)
	    F(0)
	    G(1)
	    A --- B
	    A --- C
	    B --- D
	    B --- E
	    C --- F
	    C --- G
    ```
    </div>

!!! info "Un arbre horizontal avec des formules de mathématiques"

    Il suffit d'écrire les formules en LaTex à l'intérieur de `"$$...$$"`

    ````markdown title="Le code à copier"
    ```mermaid
    graph LR
        O( )
        A(A)
        B(B)
        C(C)
        E(X)
        F(Y)
        G(X)
        H(Y)
        I(X)
        J(Y)
        O --- |"$$P(A)$$"| A 
        O --- |"$$P(B)$$"| B
        O --- |"$$P(C)$$"| C
        A --- |"$$P_{A}(X)$$"| E
        A --- |"$$P_{A}(Y)$$"| F
        B --- |"$$P_{B}(X)$$"| G
        B --- |"$$P_{B}(Y)$$"| H
        C --- |"$$P_{C}(X)$$"| I
        C --- |"$$P_{C}(Y)$$"| J
    ```
    ````

    <div class="result" markdown>

    ```mermaid
    graph LR
        O( )
        A(A)
        B(B)
        C(C)
        E(X)
        F(Y)
        G(X)
        H(Y)
        I(X)
        J(Y)
        O --- |"$$P(A)$$"| A 
        O --- |"$$P(B)$$"| B
        O --- |"$$P(C)$$"| C
        A --- |"$$P_{A}(X)$$"| E
        A --- |"$$P_{A}(Y)$$"| F
        B --- |"$$P_{B}(X)$$"| G
        B --- |"$$P_{B}(Y)$$"| H
        C --- |"$$P_{C}(X)$$"| I
        C --- |"$$P_{C}(Y)$$"| J
    ```
    </div>



## XI. Les graphes

!!! info "graphe orienté"
    ````markdown title="Le code à copier"
    ```mermaid
        graph LR
        A[texte 1] ----> |50| C[texte 2];
        A -->|1| B[B];
    ```
    ````
    <div class="result" markdown>

    ```mermaid
        graph LR
        A[texte 1] ----> |50| C[texte 2];
        A -->|1| B[B];
    ```
    </div>


!!! info "graphe orienté horizontal"

    ````markdown title="Le code à copier"
    ```mermaid
        graph LR
            A[Editeur de texte PHP] --> C[Serveur Apache]
            C --> B[Navigateur]
    ```
    ````
    <div class="result" markdown>

    ```mermaid
        graph LR
            A[Editeur de texte PHP] --> C[Serveur Apache]
            C --> B[Navigateur]
    ```
    </div>


!!! info "graphe non orienté"

    ````markdown title="Le code à copier"
    ```mermaid
        graph LR
            A --- |1| B
            B --- |5| C
            A --- |5| D
            B --- |1| D
            D --- |1| C
            A --- |50| C
    ```
    ````
    <div class="result" markdown>

    ```mermaid
        graph LR
            A --- |1| B
            B --- |5| C
            A --- |5| D
            B --- |1| D
            D --- |1| C
            A --- |50| C
    ```
    </div>

!!! info "graphe orienté avec des nœuds circulaires ou carrés"

    ````markdown title="Le code à copier"
    ```mermaid
        graph LR
            A[Fichier] --> D((D))
            D --> C[Clavier]
            C --> B((B))
            B --> A
    ```
    ````
    <div class="result" markdown>

    ```mermaid
        graph LR
            A[Fichier] --> D((D))
            D --> C[Clavier]
            C --> B((B))
            B --> A
    ```
    </div>

!!! bug "Bug de la version actuelle de mermaid"

    Depuis la version 11 mermaid met des flèches à toutes les arêtes, même avec la syntaxe `A --- B`

    Vous pouvez recopier le code qui ne fonctionne plus ici : [Lien vers Play](https://www.mermaidchart.com/play){ .md-button target="_blank" rel="noopener" }

    Il suffit ensuite de choisir "Exporter" puis "image au format SVG" dans la barre d'outil verticale à gauche. Il faut ensuite utiliser cette image ([Les images au I. 4.](https://docs.forge.apps.education.fr/tutoriels/tutoriel-site-simple/02_basique/2_page_basique/#i-syntaxe-basique-de-markdown){:target="_blank" }) 

[Lien vers différentes options de nœuds et flèches](https://coda.io/@leandro-zubrezki/diagrams-and-visualizations-using-mermaid/flowcharts-3){ .md-button target="_blank" rel="noopener" }


[lien vers la documentation mermaid - essais en ligne de mermaid](https://www.mermaidchart.com/play){ .md-button target="_blank" rel="noopener" }


## XII. Les classes

!!! info "Une classe"

    ````markdown title="Le code à copier"
    ```mermaid
        classDiagram
        class Yaourt{
            str arome
            int duree
            str genre
            __init__(arome,duree)
            modifie_duree(duree)
            modifie_arome(arome)
        }
    ```
    ````
    <div class="result" markdown>

    ```mermaid
        classDiagram
        class Yaourt{
            str arome
            int duree
            str genre
            __init__(arome,duree)
            modifie_duree(duree)
            modifie_arome(arome)
        }
    ```
    </div>

[lien vers la documentation mermaid - essais en ligne de mermaid](https://mermaid.live/edit#pako:eNpVkE1qw0AMha8itGohvoAXgcZus0lJodl5vBAeOTOk88NYpgTbd-84ptBqJfS-J6Q3YRc0Y4nXRNHApVYecr00lUl2EEdDC0Wxn48s4ILn-wyHp2OAwYQYrb8-b_xhhaCaTivGIMb627JJ1cN_9jxD3ZwoSojtX-XyHWZ4beyHyev_KyZxdr01PZU9FR0lqCi1uEPHyZHV-expNSgUw44VlrnV3NP4JQqVXzJKo4TPu--wlDTyDseoSbi2lB92v0PWVkJ635J4BLL8AM-3Wn4){ .md-button target="_blank" rel="noopener" }




