---
author: Mireille Coilhac
title: Ressources
---


[La documentation Pyodide Mkdocs Theme de Frédéric Zinelli](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/){ .md-button target="_blank" rel="noopener" }

[CodEx : le code par les exercices](https://codex.forge.apps.education.fr/){ .md-button target="_blank" rel="noopener" }

[Catalogue de ressources par Nathalie Bessonnet](https://bessonnetnathalie.forge.apps.education.fr/ressources-nsi/){ .md-button target="_blank" rel="noopener" }
