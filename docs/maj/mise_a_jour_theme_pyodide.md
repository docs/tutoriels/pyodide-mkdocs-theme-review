---
author: Mireille Coilhac
title: Mise à jour vers pyodide-mkdocs-theme
---

!!! info "Passage de mkdocs-pyodide à pyodide-mkdocs-theme"

    Beaucoup de sites avec Python interactif ont été développés en utilisant mkdocs-pyodide créé par Vincent Bouillot.  
    Le passage à pyodide-mkdocs-theme écrit par Frédéric Zinelli, permet beaucoup d'évolutions présentées ci-dessous, et corrige des bugs.


<div style="display:flex;gap:2em;align-items:top">
<svg viewBox="0 0 512 512"
    height="24px" width="24px" version="1.1" xmlns="http://www.w3.org/2000/svg"
    xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" fill="gray">
    <g>
        <path class="st0" d="M329.368,237.908l42.55-39.905c25.237-23.661,39.56-56.701,39.56-91.292V49.156 c0.009-13.514-5.538-25.918-14.402-34.754C388.24,5.529,375.828-0.009,362.314,0H149.677c-13.514-0.009-25.918,5.529-34.754,14.401 c-8.872,8.837-14.41,21.24-14.402,34.754v57.554c0,34.591,14.315,67.632,39.552,91.292l42.55,39.888 c2.352,2.205,3.678,5.272,3.678,8.493v19.234c0,3.221-1.326,6.279-3.67,8.475l-42.558,39.905 c-25.237,23.653-39.552,56.702-39.552,91.292v57.554c-0.009,13.514,5.529,25.918,14.402,34.755 c8.836,8.871,21.24,14.409,34.754,14.401h212.636c13.514,0.008,25.926-5.53,34.763-14.401c8.863-8.838,14.41-21.241,14.402-34.755 v-57.554c0-34.59-14.324-67.64-39.56-91.292l-42.55-39.896c-2.344-2.205-3.678-5.263-3.678-8.484v-19.234 C325.69,243.162,327.025,240.095,329.368,237.908z M373.942,462.844c-0.009,3.273-1.266,6.055-3.403,8.218 c-2.162,2.135-4.952,3.402-8.226,3.41H149.677c-3.273-0.009-6.055-1.275-8.225-3.41c-2.128-2.163-3.394-4.945-3.402-8.218v-57.554 c0-24.212,10.026-47.356,27.691-63.91l42.55-39.906c9.914-9.285,15.538-22.274,15.538-35.857v-19.234 c0-13.592-5.624-26.58-15.547-35.866l-42.541-39.896c-17.666-16.555-27.691-39.69-27.691-63.91V49.156 c0.008-3.273,1.274-6.055,3.402-8.226c2.17-2.127,4.952-3.394,8.225-3.402h212.636c3.273,0.009,6.064,1.275,8.226,3.402 c2.136,2.171,3.394,4.952,3.403,8.226v57.554c0,24.22-10.026,47.355-27.683,63.91l-42.55,39.896 c-9.922,9.286-15.547,22.274-15.547,35.866v19.234c0,13.583,5.625,26.572,15.547,35.874l42.55,39.88 c17.658,16.563,27.683,39.707,27.683,63.918V462.844z"></path>
        <path class="st0" d="M256,248.674c10.017,0,18.131-8.122,18.131-18.139c3.032-12.051,9.397-23.161,18.578-31.757l42.542-39.888 c13.592-12.739,21.602-30.448,22.446-48.984H154.302c0.844,18.536,8.854,36.245,22.438,48.984l42.541,39.888 c9.19,8.596,15.547,19.706,18.579,31.757C237.861,240.552,245.983,248.674,256,248.674z"></path>
        <path class="st0" d="M256,267.796c-10.017,0-18.139,8.122-18.139,18.139c0,10.009,8.122,18.131,18.139,18.131 c10.017,0,18.131-8.122,18.131-18.131C274.131,275.918,266.017,267.796,256,267.796z"></path>
        <path class="st0" d="M256,332.137c-10.017,0-18.139,8.122-18.139,18.14c0,10.009,8.122,18.131,18.139,18.131 c10.017,0,18.131-8.122,18.131-18.131C274.131,340.259,266.017,332.137,256,332.137z"></path>
        <path class="st0" d="M239.876,389.742l-66.538,66.538h165.315l-66.537-66.538C263.21,380.845,248.782,380.845,239.876,389.742z"></path>
    </g>
</svg> <div class='gray'>Nouveau après la mise à jour : 
<br>Tant que ce sablier (animé) est présent dans le bandeau supérieur, des éléments de la page sont inactifs :<br>
l'environnement pyodide est en cours de démarrage.</div>
</div>

## I. Pourquoi faire une MAJ ?

!!! info "Une Mise à jour importante"

    Pyodide-MkDocs-Theme est un thème pour MkDocs, créé par Frédéric Zinelli.

    - Correction des bugs de la version précédantes de pyodide, fonctionalités rajoutées.  
    - Exercices beaucoup plus faciles à écrire, avec seul fichier python (entre 1 et 4 dans la version précédante)
    - Exécution du code beaucoup plus rapide, ce qui est sensible pour les fonctions récursives et les boucles importantes.
    - facilité d'installation et de maintenance

    Ce thème permet de construire des sites pouvant intégrer, côté client:

    - des IDE (éditeur de code),
    - des consoles python interactives,
    - un juge en ligne pour tester des fonctions rédigée par l'utilisateur, associé à des corrections et remarques,
    - et quelques autres fonctionnalités variées (qcms, ...).

    😊 Une fois la mise à jour effectuée, le site fonctionnera encore avec l'ancienne structure (plusieurs fichiers python pour un IDE par exemple).
    Il n'y a donc aucune inquiétude à avoir pour réaliser cette mise à jour.


    ??? danger "Attention pour les qcm, les macros personelles, et l'option de taille d'IDE"

        * Si des syntaxes (expliqué dans le III):   

        ```markdown
        {% raw %}
        {{ qcm( ... ) }}
        {% endraw %}
        ```
        ont été utilisées dans le site, il faudra les supprimer, et faire les modifications pour utiliser :
        
        ```markdown
        {% raw %}
        {{ multi_qcm( ... ) }}
        {% endraw %}
        ```

        👉 Il est recommandé de faire une recherche des qcm sur votre site, avant MAJ, grâce à la barre de recherche située en haut à droite. Il faudra
         noter les pages qui contiennent des qcm, pour faire les modifications éventuelles.

        * Si vous avez réalisé des macros personnelles, il faudra être attentif à ne pas les perdre (expliqué dans le III).

        * Si vous avez utilisé :

        ```markdown
        {% raw %}
        {{ IDE( 'exo', SIZE=45 ) }}
        {% endraw %}
        ```

        Il faudra le remplacer par 

        ```markdown
        {% raw %}
        {{ IDE( 'exo', MAX_SIZE=45 ) }}
        {% endraw %}
        ```
        
        Sinon cela mettra le pipeline en échec.


!!! warning "Remarque"

    Si vous n'avez jamais construit de site avec Python (pyodide) cette mise à jour ne vous est pas destinée. Le tutoriel présenté ici, est **très simplifié**, car il est destiné à la mise à jour de sites créés en utilisant le modèle proposé dans ce tutoriel
    ancien : [Ancien tutoriel pour créer son site](https://tutoriels.forge.apps.education.fr/mkdocs-pyodide-review/){:target="_blank" }

    **Ce tutoriel s'adresse à des débutants avec GitLab.** 


    👉 Pour des points particuliers, se référer au lien  [Documentation officielle pour la MAJ depuis les sites créés avec pyodide-mkdocs](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/maj_pyodide_mkdocs/){ .md-button target="_blank" rel="noopener" }

    


!!! warning

    L'essentiel des fonctionnalités de [pyodide-mkdocs](https://bouillotvincent.gitlab.io/pyodide-mkdocs/){: target=_blank } restent présentes dans 
    [pyodide-mkdocs-theme](https://pypi.org/project/pyodide-mkdocs-theme/){: target=_blank }.


    [Documentation officielle de Pyodide Mkdocs Theme](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/){ .md-button target="_blank" rel="noopener" }


    Cependant, certaines modifications ont dû être apportées au projet, qui font que le passage à la version "thème" nécessitera quelques changements, 
    aussi bien du côté de l'environnement (packages installés) que de la configuration et du contenu.

## II. Créer une copie de sauvegarde de votre projet

!!! danger "Indispensable avant de commencer"

    Faire un `fork` de votre projet, et le renommer (par exemple mon projet old). Vous allez modifier par la suite votre projet d'origine.

    [faire un fork](../08_tuto_fork/1_fork_projet.md){ .md-button target="_blank" rel="noopener" }



!!! warning

    Le rendu du site ne sera possible qu'après avoir terminé **toutes** les modifications à réaliser.

## III. Supprimer ou modifier des fichiers.

Une fois cette copie de sauvegarde effectuée, revenir sur le projet à mettre à jour.

!!! info "Supprimez le dossier `my_theme_customizations`"

    Supprimez le dossier `my_theme_customizations` et tout ce qu'il contient avec un clic droit sur le dossier.


!!! info "Modifier ou supprimer le fichier `main.py`"


    !!! warning "Très important si vous avez utilisé des macros personnelles"

        Ne pas faire ce qui est proposé dans l'encadré qui suit, mais se reporter ici :

        * [Documentation officielle de MAJ pour les macros](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/custom/extra_macros/){:target="_blank" }
        * [Documentation pour réincorporer vos personnalisations](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/maj_pyodide_mkdocs/#6-reincorporer-vos-personnalisations){:target="_blank" }



    !!! info "Si vous n'avez pas utilisé des macros personnelles"

        Supprimer le fichier `main.py`


!!! info "Suppressions dans le dossier `docs`"

    Dans le dossier `docs` :

    * supprimer le dossier `javascripts` et tout ce qu'il contient
    * Dans le dossier `xtra`supprimer le dossier `javascripts` et tout ce qu'il contient.
    * Dans le dossier `xtra` dans le dossier `stylesheets` supprimer le fichier `qcm.css`

!!! info "Modifications du fichier `mkdocs.yml`"

    Dans le fichier `mkdocs.yml`

    * Vérifier la présence des lignes suivantes (les modifier si nécessaire)

    ```yaml
    site_url: !ENV [CI_PAGES_URL, "http://127.0.0.1:8000/"]
    site_author: !ENV [CI_COMMIT_AUTHOR, "Nom d'auteur"]
    repo_url: !ENV [CI_PROJECT_URL]
    edit_uri: !ENV [EDIT_VARIABLE]
    ```

    * Vers la ligne n° 22 : section `theme`, supprimer (mettre des `#` en début de ligne) comme ci-dessous.

    * Vers la ligne n° 27 remplacer `name: material` par **`name: pyodide-mkdocs-theme`**

    * Vers la ligne n° 47 ne pas oublier de supprimer `navigation.instant`, car le thème **n’est pas compatible** avec cette option.

    ```yaml title=""
    theme:
        favicon: assets/favicon.ico
        icon:
        logo: material/stairs-up
        #custom_dir: my_theme_customizations/
        name: pyodide-mkdocs-theme
        #font: false                     # RGPD ; pas de fonte Google
        #language: fr                    # français
        #palette:                        # Palettes de couleurs jour/nuit
        #- media: "(prefers-color-scheme: light)"
            #scheme: default
            #primary: indigo
            #accent: indigo
            #toggle:
                #icon: material/weather-sunny
                #name: Passer au mode nuit
        #- media: "(prefers-color-scheme: dark)"
            #scheme: slate
            #primary: black
            #accent: green
            #toggle:
                #icon: material/weather-night
                #name: Passer au mode jour
        features:
            #- navigation.instant
            #- navigation.tabs   Pour avoir le menu vertical il fallait supprimer ça
            - navigation.top
            - toc.integrate
            - header.autohide
            - content.code.annotate   # Pour les annotations de code deroulantes avec +    
    ```

    * Vers la ligne n°66 section markdown_extensions: ajouter `md_in_html` au début `markdown_extensions:`
    * Vers la ligne n°89 **remplacer** 
    ```yaml title=""
        - pymdownx.emoji:               # Émojis  :boom:
            emoji_index:     !!python/name:materialx.emoji.twemoji
            emoji_generator: !!python/name:materialx.emoji.to_svg
    ``` 

    **par** : 
    ```yaml title=""
        - pymdownx.emoji: # Émojis  :boom:
            emoji_index: !!python/name:material.extensions.emoji.twemoji
            emoji_generator: !!python/name:material.extensions.emoji.to_svg
    ```

    ```yaml title=""
    markdown_extensions:
        - md_in_html
        - meta
        - abbr
        - def_list                      # Les listes de définition.
        - attr_list                     # Un peu de CSS et des attributs HTML.
        - footnotes                     # Notes[^1] de bas de page.  [^1]: ma note.
        - admonition                    # Blocs colorés  !!! info "ma remarque"
        - pymdownx.details              #   qui peuvent se plier/déplier.
        - pymdownx.caret                # Passage ^^souligné^^ ou en ^exposant^.
        - pymdownx.mark                 # Passage ==surligné==.
        - pymdownx.tilde                # Passage ~~barré~~ ou en ~indice~.
        - pymdownx.highlight:           # Coloration syntaxique du code
        - pymdownx.inlinehilite         # pour  `#!python  <python en ligne>`
        - pymdownx.snippets             # Inclusion de fichiers externe.
        - pymdownx.tasklist:            # Cases à cocher  - [ ]  et - [x]
            custom_checkbox:    false   #   avec cases d'origine
            clickable_checkbox: true    #   et cliquables.
        - pymdownx.tabbed:              # Volets glissants.  === "Mon volet"
            alternate_style: true 
        - pymdownx.keys:                # Touches du clavier.  ++ctrl+d++
            separator: "\uff0b"
        - pymdownx.emoji: # Émojis  :boom:
            emoji_index: !!python/name:material.extensions.emoji.twemoji
            emoji_generator: !!python/name:material.extensions.emoji.to_svg
        - pymdownx.superfences:
            custom_fences:
            - name: mermaid
                class: mermaid
                format: !!python/name:pymdownx.superfences.fence_code_format
        - pymdownx.arithmatex:
            generic: true
        - toc:
            permalink: ⚓︎
            toc_depth: 2
    ```   

    * Vers la ligne n°103 section `plugins`, vous devez avoir ceci, **dans le même ordre** : 

    ```yaml title="A copier"
    plugins:

        - awesome-pages:
            collapse_single_pages: true

        - search
        - tags:
            tags_file: tags.md
        - pyodide_macros:
            # Vous pouvez ajouter ici tout réglage que vous auriez ajouté concernant les macros:
            on_error_fail: true     # Il est conseillé d'ajouter celui-ci si vous ne l'utilisez pas.
            build:
              python_libs:  # Pour avoir la tortue Python
                - turtle
              tab_to_spaces: 4  # Pour convertir les tabulations en 4 espaces


    # En remplacement de mkdocs-exclude. Tous les fichiers correspondant aux patterns indiqués seront
    # exclu du site final et donc également de l'indexation de la recherche.
    # Nota: ne pas mettre de commentaires dans ces lignes !
    exclude_docs: |
        **/*_REM.md
        **/*.py
    ```



    * Vers les lignes n°130 : supprimer (mettre des `#` en début de ligne) comme ci-dessous.

    ```yaml 
    #extra_javascript:
    #- xtra/javascripts/mathjax-config.js       Supprimé pour MAJ pyodide             
    #- https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js

    extra_css:
      #- xtra/stylesheets/qcm.css ##       Supprimé pour MAJ pyodide  
      - xtra/stylesheets/ajustements.css  # ajustements
    ```

!!! info "Ajouter le répertoire `turtle`"

    Il faut mettre le dossier `turtle` à la racine du site à télécharger ici :  [Clic droit, puis "Enregistrer la cible du lien sous"](../bibs_graphiques/a_telecharger/turtle.zip)  
    Décompresser le fichier `turtle.zip` puis mettre le dossier `turtle` à la racine du projet.

    ![turtle](images/telecharger_turtle.png){ width=50% }

    Vous obtiendrez : 

    ![turtle à la racine](images/ajout_turtle.PNG){ width=20% }

!!! info "Modifications du fichier `requirements.txt`"

    Remplacer le contenu du fichier `requirements.txt` par :

    ```txt title="code à copier"
    pyodide-mkdocs-theme
    mkdocs-awesome-pages-plugin
    mkdocs-enumerate-headings-plugin
    mkdocs-exclude-search
    ```

!!! info "Le fichier `tags.md`"

    Dans le dossier `docs` se trouve en principe un fichier `tags.md`

    Si ce n'est pas le cas, le rajouter : 

    ```markdown title="code à copier pour le fichier tags.md"
    # 🏷️ Tags

    ```

!!! info "Les QCM"

    Si des syntaxes (expliqué dans le III):   

    ```markdown
    {% raw %}
    {{ qcm( ... ) }}
    {% endraw %}
    ```
    ont été utilisées dans le site, il faudra les supprimer, et faire les modifications pour utiliser :
        
    ```markdown
    {% raw %}
    {{ multi_qcm( ... ) }}
    {% endraw %}
    ```

    !!! info "Exemple de code à supprimer"

        👉 Par exemple, il faut supprimer ceci : 

        ```markdown
        Question 1 : $2^3=$
        {% raw %}
        {{ qcm(["9", "8", "Je ne sais pas", "6"], [2], shuffle = True) }}
        {% endraw %}
        ```

    !!! info "Exemple de code de remplacement"

        On peut remplacer le code précédant par :

        !!! info "Version longue"

            ```markdown title="code à copier"
            {% raw %}
            {{ multi_qcm(
                [
                    "Compléter : $2^3=$",
                    [
                        "9",
                        "8",
                        "Je ne sais pas",
                        "6",
                    ],
                    [2],
                ],

                multi = False,
                qcm_title = "Puissances",
                shuffle = True,
            ) }}
            {% endraw %}
            ```

        !!! info "Version courte"

            On peut aussi tout simplement écrire de façon plus concise : 

            ```markdown title="code à copier"
            {% raw %}
            {{ multi_qcm(
                ["Compléter : $2^3=$", ["9", "8", "Je ne sais pas", "6"],[2],],
                multi = False,
                qcm_title = "Puissances",
                shuffle = True,
            ) }}
            {% endraw %}
            ```          

    !!! info "Tout savoir sur les qcm"

        Se reporter ici : [Documentation officielle sur les qcm](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/redactors/qcms/){ .md-button target="_blank" rel="noopener" }

!!! info "Les noms de dossiers et de fichiers"

    Attention, les seuls caractères autorisés pour les noms de fichiers sont : les lettres, chiffres, traits d’union ou traits bas, et rien d’autre. 

    Les caractères spéciaux, espaces et lettres accentuées sont **interdits**. 
    
    👉 Penser à  modifier ces noms si nécessaire.

!!! info "Les fichiers .pages"

    La syntaxe `arrange` va devenir obsolète. Dans les fichiers `.pages` remplacer `arrange` par `nav`.

    ````markdown title="Code exemple pour un fichier `.pages`"
    title: Mon titre
    nav:
        - page1.md
        - page2.md
        - repertoire_1
        - page3.md
        - repertoire_2
    ````


## IV. Terminer

!!! info "Faire le commit"

    Faire le commit, et vérifier le rendu du site.

## V. En cas d'erreur

!!! failure "En cas d'échec du pipeline"

    ![echec pipeline](images/echec_pipeline.png){ width=70% }

    😅 Ce n'est pas très grave, pas de panique !

    ![cliquer echec](images/cliquer_echec.png){ width=60% }

    Vous obtenez quelquechose comme ceci : 

    ![en echec](images/en_echec.png){ width=70% }

    Cliquer sur J"obs ayant échoué" puis lire le message qui s'affiche.

    ![message echec](images/message_echec_2.png){ width=90% }

    😏 Il n'y a plus qu'à comprendre le message d'erreur (c'est un peu effrayant, mais cela permet de résoudre le problème), et rectifier avant de faire un nouveau commit ...

    Dans l'exemple ci-dessus on lit : `Nav entry "fractales" not found. [.pages]` . En effet, dans le dfichier `.pages` je fais référence à un dossier 
    "fractales" qui avait été supprimé du site. Il suffit de supprimer la ligne correspondante du fichier `.pages`, de faire un commit, et le nouveau pipeline ne sera pas en échec.


    !!! info "Erreurs fréquentes qui mettent un pipeline en échec"

        * Une macro qui n'existe pas (voir pour les qcm par exemple) met le pipeline en échec.
        * Utiliser une section vide dans un fichier python : [Sections vides](https://docs.forge.apps.education.fr/tutoriels/pyodide-mkdocs-theme-review/03_ecrire_exos/exercices/#xi-cause-frequente-de-pannes){ .md-button target="_blank" rel="noopener" }
        * Une erreur possible : utilisation de l'ancienne option `SIZE` pour la taille d'un IDE. Il faut le remplacer par `MAX_SIZE` (Voir le I.)
        * Un fichier `.pages` incorrect
        * Un nom de fichier est invalide : Le thème n’autorise plus les caractères accentués, caractères spéciaux, espaces …   
        Voir : [Mettre à jour les fichiers](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/maj_pyodide_mkdocs/#7-mettre-a-jour-les-fichiers-de-la-documentation){:target="_blank" }
    

## Crédits

Merci à Frédéric Zinelli qui a créé ce thème, et une documentation très détaillée, dont je me suis très largement inspirée.

[Documentation détaillée de pyodide-mkdocs-theme par F. Zinelli](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/redactors/resume/){ target="_blank"  }
