---
author: Mireille Coilhac
title: Images et animations en Python
---


!!! info "Utiliser les bibliothèques graphiques"

    Utiliser les bibliothèques graphiques n'est pas tout à fait immédiat.

    Voici trois exemples.


## I. Utiliser la bibliothèque matplotlib 

!!! info "Le principe"

    !!! info "Le fichier Python"

        Le fichier Python dans lequel se trouve le code qui permet de tracer la figure désirée 
        peut contenir une section : 
        
        `# --- PYODIDE:env --- #`
        
        Dans cette section on fera ___en premier___ l'import `import matplotlib.pyplot as plt` et on on créera des objets `PyodidePlot `
        qui permettront ensuite d'utiliser les sytaxes habituelles de matplotlib avec `plt` et de tracer les figures aux bons endroits dans la page.


    !!! info "Le fichier Markdown"

        Le principe est d'appeler : 

        * Une fois la macro
        ```markdown title=""
        {% raw %}
        {{ IDE(...) }}
        {% endraw %}
        ```

        * Autant de fois que de fenêtres graphiques désirées, la macro :
        ```markdown title=""
        {% raw %}
        {{ figure(...) }}
        {% endraw %}
        ``` 
    
        * Dans le cas où l'on désire plusieurs fenêtres, il faut les identifier. On appelera donc 
        par exemple pour deux fenêtres :  
        ```md title=""
        {% raw %}
        {{ figure('cible_1') }}

        {{ figure('cible_2') }}
        {% endraw %}
        ```

    
### Tracé d'un seul graphique


    
```markdown title="Code Markdown dans le fichier .md"
{% raw %}
{{ IDE('scripts/fct_carre') }}

{{ figure() }}
{% endraw %}
```


{{ IDE('scripts/fct_carre') }}

{{ figure() }}

??? note pliée "Fichier python utilisé pour cet exemple"

    ```python title="fct_carre.py"
    # --- PYODIDE:env --- #

    # Un import de matplotlib en tout premier est indispensable, pour que la classe
    # PyodidePlot devienne disponible dans l'environnement:
    import matplotlib.pyplot as plt
    PyodidePlot().target()     # Cible la figure dans laquelle tracer la figure dans la page

    # --- PYODIDE:code --- #
    # L'import suivant a été fait dans du code caché : 
    # import matplotlib.pyplot as plt

    xs = [-3 + k * 0.1 for k in range(61)]
    ys = [x**2 for x in xs]
    plt.plot(xs, ys, "r-")
    plt.grid()  # Optionnel : pour voir le quadrillage
    plt.axhline()  # Optionnel : pour voir l'axe des abscisses
    plt.axvline()  # Optionnel : pour voir l'axe des ordonnées
    plt.title("La fonction carré")
    plt.show()
    ```

### Tracé de deux graphiques différents dans deux fenêtres différentes


    
```markdown title="Code Markdown dans le fichier .md"
{% raw %}
{{ IDE('scripts/fct_carre_cube') }}

{{ figure('cible_1') }}

{{ figure('cible_2') }}
{% endraw %}
```

{{ IDE('scripts/fct_carre_cube') }}
 
{{ figure('cible_1') }}

{{ figure('cible_2') }}

??? note pliée "Fichier Python utilisé pour cet exemple"

    ```python title="fct_carre_cube.py"
    # --- PYODIDE:env --- #

    import matplotlib.pyplot as plt
    fig1 = PyodidePlot('cible_1')  
    fig2 = PyodidePlot('cible_2')

    # --- PYODIDE:code --- #
    # L'import suivant a été fait dans du code caché : 
    # import matplotlib.pyplot as plt

    fig1.target() # Pour tracer la fonction ci-dessous
    xs = [-3 + k * 0.1 for k in range(61)]
    ys = [x**2 for x in xs]
    plt.plot(xs, ys, "r-")
    plt.grid()  # Optionnel : pour voir le quadrillage
    plt.axhline()  # Optionnel : pour voir l'axe des abscisses
    plt.axvline()  # Optionnel : pour voir l'axe des ordonnées
    plt.title("La fonction carré")
    plt.show()

    fig2.target() # Pour tracer la fonction ci-dessous
    xs = [-2 + k * 0.1 for k in range(41)]
    ys = [x**3 for x in xs]
    plt.plot(xs, ys, "r-")
    plt.grid()  # Optionnel : pour voir le quadrillage
    plt.axhline()  # Optionnel : pour voir l'axe des abscisses
    plt.axvline()  # Optionnel : pour voir l'axe des ordonnées
    plt.title("La fonction cube")
    plt.show()
    ```

### Tracé de deux graphiques superposés dans la même fenêtre

    
```markdown title="Code Markdown dans le fichier .md"
{% raw %}
{{ IDE('scripts/carre_cube_superposees') }}

{{ figure('cible_double') }}
{% endraw %}
```

{{ IDE('scripts/carre_cube_superposees') }}

{{ figure('cible_double') }}
    
??? note pliée "Fichier Python utilisé pour cet exemple"

    ```python title="carre_cube_superposees.py"
    # --- PYODIDE:env --- #

    import matplotlib.pyplot as plt         # Indispensable (provoque la déclaration de PyodidePlot)
    fig = PyodidePlot('cible_double')  
    fig.target()

    # --- PYODIDE:code --- #
    # L'import suivant a été fait dans du code caché : 
    # import matplotlib.pyplot as plt


    xs1 = [-3 + k * 0.1 for k in range(61)]
    ys1 = [x**2 for x in xs1]

    xs2 = [-2 + k * 0.1 for k in range(41)]
    ys2 = [x**3 for x in xs2]

    plt.plot(xs1, ys1, "r-", xs2, ys2, "b+")  
    plt.grid()  # Optionnel : pour voir le quadrillage
    plt.axhline()  # Optionnel : pour voir l'axe des abscisses
    plt.axvline()  # Optionnel : pour voir l'axe des ordonnées
    plt.title("La fonction carré et la fonction cube")
    plt.show()
    ```
    


!!! info "Documentation détaillée"

    [Documentation officielle pour l'utilisation de Matplotlib](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/custom/matplotlib/){ .md-button target="_blank" rel="noopener" } 


!!! danger "Attention"

    Il ne faut pas utiliser plusieurs fois le même identifiant dans une page du site, par exemple ici l'identifiant `cible_1`
    ```markdown title=""
    {% raw %}
    {{ figure('cible_1') }}
    {% endraw %}
    ``` 

    L’appel sans argument (ci-dessous) utilise également `#!py "figure1"` comme valeur par défaut pour identifier une figure, 
    donc il ne peut être utilisé qu’**une seule fois par page**, et les autres appels ne doivent pas utiliser le même identifiant.
    ```markdown title=""
    {% raw %}
    {{ figure() }}
    {% endraw %}
    ``` 

## II. Réaliser des animations avec P5

[Documentation officielle de Frédéric Zinelli](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/custom/p5/){ .md-button target="_blank" rel="noopener" }


## III. Utiliser la tortue par Romain Janvier


!!! danger "Pour pouvoir utiliser la bibliothèque turtle"

    Si votre site a été créé à partir du site modèle de ce tutoriel après le 1er novembre 2024, tout fonctionnera correctement.  
    Sinon, vérifier les points suivants (le dossier turtle à télécharger a été modifié le 1er novembre 2024) : 

    ??? note "Mise à jour - points à vérifier"

        * Il faut rajouter dans le dossier `mkdocs.yml` : 
        ```yaml 
        plugins:
          - pyodide_macros:
              build:
                python_libs:
                  - turtle
        ```

        * Il faut mettre le dossier `turtle` à la racine du site à télécharger ici :  [Clic droit puis "Enregistrer la cible du lien sous"](a_telecharger/turtle.zip)
        Décompresser le fichier `turtle.zip` puis mettre le dossier `turtle` à la racine du projet.

        ![turtle](telecharger_turtle.png){ width=50% }

        Vous obtiendrez : 

        ![turtle à la racine](ajout_turtle.PNG){ width=20% }

!!! warning "Remarque : exécuter deux fois un script"

    Avec la version actuelle, si on exécute une fois un script, la figure apparaît en une seule fois. 
    Si on exécute à nouveau ce même script, on voit la tortue se déplacer.


```markdown title="Code Markdown dans le fichier .md"
???+ question "Utilisation de la tortue"

    {% raw %}
    {{ IDE('scripts/arbre_tortue') }}

    {{ figure('cible_3') }}
    {% endraw %}   
```


???+ question "Utilisation de la tortue"

    {{ IDE('scripts/arbre_tortue') }}
    
    {{ figure('cible_3') }}

??? note "Fichier Python utilisé pour cet exemple"


    ```python title="arbre_tortue.py"
    # --------- PYODIDE:env --------- #
    from js import document
    if "restart" in globals():
        restart()

    def m_a_j(cible):
        done()
        document.getElementById(cible).innerHTML = Screen().html

    _cible = 'cible_3'

    # --------- PYODIDE:code --------- #
    from turtle import *
    setup(640, 480)  # pour définir la taille de la fenêtre
    speed(10)

    def arbre(l=100, n=5):
        forward(l)
        if n > 0:
            left(45)
            arbre(l/2, n-1)
            right(90)
            arbre(l/2, n-1)
            left(45)
        back(l)

    arbre(200, 5)

    # --------- PYODIDE:post --------- #
    if Screen().html is None:
        forward(0)
    m_a_j(_cible)

    # --------- PYODIDE:post_term --------- #
    if "m_a_j" in globals():
        m_a_j(_cible)  
    ```

!!! info "À recopier dans votre fichier Python"

    👉 Il faut juste bien **adapter** la section `# --- PYODIDE:env --- #` avec le nom de l'id utilisé dans le fichier md :  
    
    Ici `_cible = 'cible_3'`   
    
    correspond à :  
    
    ```markdown title=""
    {% raw %}
    {{ figure('cible_3') }}
    {% endraw %}
    ```


    ```python title="Code à copier"
    # --------- PYODIDE:env --------- #
    from js import document
    if "restart" in globals():
        restart()

    def m_a_j(cible):
        done()
        document.getElementById(cible).innerHTML = Screen().html

    _cible = 'cible_3' # adapter cette ligne

    # --------- PYODIDE:code --------- #
    from turtle import *
    # Votre code avec turtle 


    # --------- PYODIDE:post --------- #
    if Screen().html is None:
        forward(0)
    m_a_j(_cible)

    # --------- PYODIDE:post_term --------- #
    if "m_a_j" in globals():
        m_a_j(_cible)
    ```



*Crédit pour la réalisation de l'utilisation de  turtle : Romain Janvier*


