---
author: Mireille Coilhac
title: Ajouts sur ce site
---

Voici les ajouts notables réalisés sur ce site depuis sa publication.

* 26/5/2024 : Ajout de l'utilisation de matplotlib et de turtle
* 06/06/2024 : Comment cacher un répertoire dans le menu de navigation
* 11/06/2024 : syntaxe simple pour insérer une vidéo [Insérer des vidéos](https://docs.forge.apps.education.fr/tutoriels/tutoriel-site-basique/02_basique/2_page_basique/#iv-insertion-de-video){:target="_blank" }
* 16/06/2024 : la  FAQ sur une page qui n'apparaît pas dans le menu [FAQ](./erreurs/erreurs_frequentes.md){:target="_blank" }
* 18/ 06 /2024 : comment supprimer un commit (revert) [à la fin du V.](https://docs.forge.apps.education.fr/tutoriels/pyodide-mkdocs-theme-review/10_survie/kit_gitlab/#v-faire-un-commit-pour-un-nouveau-repertoire-le-telechargement-de-fichiers-la-creation-dun-fichier){:target="_blank" }
* 18/06/2024 : glisser-déposer des fichiers [Kit de survie GitLab](./10_survie/kit_gitlab.md){:target="_blank" }
* 18/06/2024 : voir les modifications d'un commit [Kit de survie GitLab](./10_survie/kit_gitlab.md){:target="_blank" }
* 18/06/2024 : visualiser le rendu du code markdown [Kit de survie GitLab](./10_survie/kit_gitlab.md){:target="_blank" }
* 20/06/2024 : formules en LaTeX pour les maths et la chimie [LaTeX](./latex/formules_latex.md){:target="_blank" }
* 05/07/2024 : parcours [pas à pas](./parcours/pas_a_pas.md){:target="_blank" }
* 25/08/2024 : mise à jour nécessaire suite à la mise à jour du thème PMT (passage à Pyodide MkDocs Theme v.2.2.0) [FAQ](./erreurs/erreurs_frequentes.md){:target="_blank" }
* 29/8/2024 : Comment créer des exercices avec IDE SQL [Page avec SQL](./page_sql/ajouter_IDE_sql.md){:target="_blank" }
* 08/09/2024 : précision sur la FAQ des IDE vides [FAQ](./erreurs/erreurs_frequentes.md){:target="_blank" }
* 31/10/2024 : Ecrire des formules de mathématiques dans un arbre [Arbres et formules de maths](https://docs.forge.apps.education.fr/tutoriels/pyodide-mkdocs-theme-review/02_basique/2_page_basique/#x-les-arbres){:target="_blank" }
* 31/10/2024 : Animations P5 [Utilisation de p5 pour les animations](https://docs.forge.apps.education.fr/tutoriels/pyodide-mkdocs-theme-review/bibs_graphiques/bib_images/#ii-realiser-des-animations-avec-p5){:target="_blank" }
* 01/11/2024 : Mise à jour de la rubrique  [Images et animations en Python](./bibs_graphiques/bib_images.md){:target="_blank" }
* 22/12/2024 : intégrer des exercices interactifs avec WIMS [À la fin du VIII.](./02_basique/2_page_basique.md){:target="_blank" }
* 27/01/2025 : Ecrire facilement des QCM en utilisant des fichiers .json [Dans le V.](./02_basique/2_page_basique.md){:target="_blank" }
