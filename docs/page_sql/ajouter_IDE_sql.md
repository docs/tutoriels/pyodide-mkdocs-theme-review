---
author: Mireille Coilhac
title: Des pages avec SQL
---


Des exemples sur le site modèle :  [Page avec SQL du site modèle](https://docs.forge.apps.education.fr/modeles/pyodide-mkdocs-theme-review/avec_SQL/exercices_sql/){:target="_blank" }


!!! info "La structure du site"


    Dans le fichier `mkdocs.yml` vers la ligne 125, dans plugins il doit y avoir `- sqlite-console` absolument après : `- pyodide_macros`

    ![Sous plugins](plugins.png){ width=50% }

    * Dans le fichier `.gitlab-ci.yml` environ à partir de la ligne 15, modifier si nécessaire pour avoir ceci :
    ```markdown title=""
    build:
    stage: build
    before_script:
        - python -m venv .venv
        - source .venv/bin/activate
        - pip install --upgrade pip
        - pip install -r requirements.txt
    ```

    * Dans le fichier `requirements.txt` ajouter la dernière ligne

    ```markdown title="Fichier requirements.txt"
    pyodide-mkdocs-theme
    mkdocs-awesome-pages-plugin
    mkdocs-enumerate-headings-plugin
    mkdocs-exclude-search
    mkdocs-sqlite-console
    ```
    

!!! info "Syntaxe pour utiliser un IDE avec SQL"

    Comme précisé dans la  [documentation](https://epithumia.github.io/mkdocs-sqlite-console/usage/) , il faut utiliser la syntaxe suivante: 
    ```markdown title=""
    {% raw %}
    {!{ sqlide titre="essai" espace="avec_sql"}!}
    {% endraw %}
    ```

!!! info "Documentation officielle"

    Se reporter ici : [documentation](https://epithumia.github.io/mkdocs-sqlite-console/usage/){ .md-button target="_blank" rel="noopener" }


!!! info "Précision : Intégrité des contraintes de clef étrangère"

    Par défaut SQLite ne vérifie pas l’intégrité des contraintes de clef étrangères. Il faut lui dire explicitement de le faire avec : 
    `PRAGMA foreign_keys=1;`

    ??? note "IDE sans utilisation de base de donnée (pas de `base= ` dans la macro)"

        Il suffit d'ajouter un fichier donné par `init=...` contenant `PRAGMA foreign_keys=1;`

        ```sql title="fichier init_1.sql"
        PRAGMA foreign_keys=1;
        ...
        ```

    ??? note "Avec utilisation de base de donnée"

        Base et init ne peuvent pas être utilisés en même temps

        Il suffit d'ajouter un IDE caché auto-exécuté qui indique la base de donnée utilisée (ici `livres`), et le code `PRAGMA foreign_keys=1;`  

        Exemple de code utilisé  ([dans le site modèle](https://docs.forge.apps.education.fr/tutoriels/pyodide-mkdocs-theme-review/02_basique/2_page_basique/#iii-faire-des-liens){:target="_blank" })

        ```markdown title=""
        {% raw %}
        {!{sqlide titre="avec base Livres" base="avec_SQL/bases/Livres.db" sql="avec_SQL/sql/option.sql" espace="mediatheque" autoexec hide}!}
        {% endraw %}
        ```

        ```sql title="fichier option.sql"
        PRAGMA foreign_keys=1;
        ```
  

!!! info "Chemins"

    Ne pas oublier aussi de prendre les chemins relatifs depuis le dossier `docs` pour les fichiers init, sql ou base.

[Documentation officielle](https://epithumia.github.io/mkdocs-sqlite-console/usage/){:target="_blank" }


*Crédit pour la réalisation du plugin `mkdocs-sqlite-console` et la documentation : Rafaël Lopez*