---
author: Mireille Coilhac
title: 🏡 Accueil
---


# ![magic](images/magic-wand_1fa84.png){ width=5% } Tutoriels et aides pour l'enseignant qui crée son site avec Python intégré

!!! info "Ce tutoriel"

    Ce tutoriel est conçu pour vous guider à travers les différentes étapes de la création de votre site web.

    Que vous soyez débutant ou que vous ayez déjà quelques notions, ce guide vous accompagnera de manière progressive et détaillée.  
    Vous y trouverez des explications sur la configuration initiale, la personnalisation du design, l'ajout de contenu pédagogique, et les bonnes pratiques pour maintenir votre site à jour.

    Chaque étape est illustrée avec des exemples concrets et des conseils pratiques pour vous aider à réussir votre projet web en toute simplicité.  
    Tous les exemples sont accompagnés de "code à copier" pour une intégration rapide dans votre site.

## 👉 Le modèle de site avec des exercices interactifs en Python

Ce tutoriel accompagne le modèle suivant à cloner, pour aider à leur prise en main :

[Rendu du site avec pyodide-mkdocs-theme](https://docs.forge.apps.education.fr/modeles/pyodide-mkdocs-theme-review/){ .md-button target="_blank" rel="noopener" }
[Dépôt du site avec pyodide-mkdocs-theme à cloner](https://forge.apps.education.fr/docs/modeles/pyodide-mkdocs-theme-review){ .md-button target="_blank" rel="noopener" }  

??? note "Cliquer pour voir le tutoriel pour un site simple sans exercices intéractifs Python"

    [Tutoriel pour un site simple](https://docs.forge.apps.education.fr/modeles/tutoriels/tutoriel-site-simple/){ .md-button target="_blank" rel="noopener" }  

??? danger "Attention : Mise à jour à effectuer si vous avez créé votre site avant le 26/08/2024"

    Le passage à Pyodide MkDocs Theme v.2.2.0 se fera automatiquement au prochain commit. Pour ne pas mettre le pipeline en échec, il faut dans le fichier `mkdocs.yml` vers la ligne 130 : 

    Remplacer  

    ```yaml title="À modifier"
      - material/search
      - material/tags:
          tags_file: tags.md
    ```

    par : 

    ```yaml title="À mettre à la place"
      - search
      - tags:
          tags_file: tags.md
    ```

!!! info "Un parcours pour construire votre site pas à pas"

    [Parcours pas à pas](./parcours/pas_a_pas.md)


!!! warning "Les sites compatibles avec ce tutoriel"

    Votre site n'est pas forcément compatible avec ce tutoriel

    ??? note "😥 Cliquer pour voir les sites non compatibles"

        Le contenu de ce tutoriel est inadapté si votre site a été construit à partir de l'ancienne version pyodide 
        et n'a pas été mis à jour.
        
        👉  Faire la mise à jour : [MAJ](maj/mise_a_jour_theme_pyodide.md){ .md-button target="_blank" rel="noopener" }


    ??? note "😊 Cliquer pour voir les sites compatibles"

        Le contenu de ce tutoriel est adapté si 
        
        * votre site a été construit à partir de l'ancienne version pyodide 
        et a  été mis à jour avec Pyodide Mkdocs Theme (par exemple ceci visible en bas du site à droite) :  

        👉 Le numéro de la version de **Pyodide MkDocs Theme** importe peu.

        ![version du thème possible après mise à jour](maj/images/version_theme.png){ width=25% }

        * ou s'il a été construit en utilisant le site modèle donné en haut de cette page.


## 👉 Cloner le modèle de site pour le personnaliser

Vous pouvez faire une bifurcation (on dit aussi un «fork»)  de ces modèles pour réaliser le vôtre.  
Le tutoriel pour réaliser cette bifurcation est sur ce site, dans la rubrique "Comment créer un site à partir d'un autre".

Lien direct : [Faire une «bifurcation» (fork en anglais)](08_tuto_fork/1_fork_projet.md){ .md-button target="_blank" rel="noopener" }


## 👉 Si vous débutez et pour comprendre la structure

Il est conseillé de commencer par : [Avant de démarrer](01_demarrage/1_demarrage.md/){ .md-button target="_blank" rel="noopener" }


!!! abstract "En bref"

    😀 Vous pourrez facilement recopier toutes les syntaxes depuis les différents tutoriels proposés.

    👉 Pour avoir des informations détaillées sur le fonctionnement du thème mkdocs permettant de réaliser les sites, voir la [Documentation détaillée de pyodide-mkdocs-theme par F. Zinelli ](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/){:target="_blank" }



_Dernière mise à jour le 21/02/2025_
